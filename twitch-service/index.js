var TwitchJs = require("twitch-js");
const axios = require("axios").default;
const username = process.env.TWITCH_USERNAME;
const password = process.env.TWITCH_PASSWORD;
const message = require("./message");
const responseHandler = require("./src");
const twitchJs = new TwitchJs.default({
  username,
  token: password,
  log: { level: "info" },
});
const chat = twitchJs.chat;
chat.connect().then(() => {
  chat.join(username);
  chat.say(username, message.main_connected);
});
chat.on("PRIVMSG", async (msg) => {
  if (msg.isSelf) return;
  const res = await axios.post("http://gateway-service:3000/ping", { msg });
  console.log(res.data);
  responseHandler(chat, res.data);
});
/*
msg: {
    _raw: '@badge-info=;badges=broadcaster/1,glitchcon2020/1;client-nonce=c93092de546eb92cabab80fe5a787303;color=#1E90FF;display-name=米粉企鵝;emote-only=1;emotes=emotesv2_381b5d14de4e4c6986351b8cce6c1ea7:0-9,11-20;flags=;id=1e1ab547-17f5-407f-92e0-73db85ef918c;mod=0;room-id=109848832;subscriber=0;tmi-sent-ts=1624096600383;turbo=0;user-id=109848832;user-type= :mifun72!mifun72@mifun72.tmi.twitch.tv PRIVMSG #mifun72 :f1yshaCall f1yshaCall',
    timestamp: 2021-06-19T09:56:40.383Z,
    command: 'PRIVMSG',
    event: 'PRIVMSG',
    channel: '#mifun72',
    username: '米粉企鵝',
    isSelf: false,
    message: 'f1yshaCall f1yshaCall',
    tags: {
      badgeInfo: '',
      badges: [Object],
      clientNonce: 'c93092de546eb92cabab80fe5a787303',
      color: '#1E90FF',
      displayName: '米粉企鵝',
      emoteOnly: '1',
      emotes: [Array],
      flags: '',
      id: '1e1ab547-17f5-407f-92e0-73db85ef918c',
      mod: '0',
      roomId: '109848832',
      subscriber: '0',
      tmiSentTs: '1624096600383',
      turbo: '0',
      userId: '109848832',
      userType: '',
      bits: undefined,
      emoteSets: [],
      username: '米粉企鵝',
      isModerator: false
    }
  }
}
*/
