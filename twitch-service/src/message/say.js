module.exports = (channel, args, client) => {
  const { message } = args;
  if (!message) throw Error("No message found for SAY COMMAND");
  return client.say(channel, message);
};
