module.exports = (channel, args, client) => {
  const id = args["target-msg-id"];
  if (!id) throw Error("No target-msg-id found for DELETE COMMAND");
  return client.delete(channel, id);
};
