const defaultChannel = process.env.TWITCH_USERNAME;
const commandRoutes = require("./commandRoutes");
const singleActionExecutor = async (client, singleAction) => {
  // parse single action
  const { action, args, wait, channel } = singleAction;
  //try
  if (action == "null") return;
  try {
    // execute action
    if (!commandRoutes.hasOwnProperty(action))
      throw Error(
        `Command Route for action ${action.toUpperCase()} not found.`
      );
    commandRoutes[action](channel || defaultChannel, args, client);
  } catch (err) {
    //catch error
    console.error(err);
  }
  //wait for next execution
  await new Promise((resolve) => {
    setTimeout(resolve(true), wait);
  });
};

module.exports = async (client, responseFromGateway) => {
  //check responseFromGateway is array
  if (Array.isArray(responseFromGateway)) {
    //if true
    //execute actions sync
    for (let i = 0; i < responseFromGateway.length; i++) {
      await singleActionExecutor(client, responseFromGateway[i]);
    }
    return;
  }
  //if false
  //execute action
  return await singleActionExecutor(client, responseFromGateway);
};
