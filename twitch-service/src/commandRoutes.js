const say = require("./message/say");
const deleteAPI = require("./message/delete");

module.exports = {
  say,
  delete: deleteAPI,
};
