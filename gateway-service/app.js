const app = require("express")();
var bodyParser = require("body-parser");
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
app.get("/ping", (req, res) => {
  res.sendStatus(204);
});

app.post("/ping", (req, res) => {
  console.log(req.body);
  const { msg } = req.body;
  switch (msg.message) {
    case "12356":
      return res.status(200).send({
        action: "say",
        args: {
          message: "ping",
        },
      });
    case "deleteMe":
      return res.status(200).send({
        action: "delete",
        args: {
          "target-msg-id": msg.tags.id,
        },
      });
    default:
      return res.status(200).send({
        action: "null",
        args: {
          message: "ping",
        },
      });
  }
});

app.post("/twitch/bot", async (req, res) => {
  //split incoming message
  //check if keyword presented
  // if false, drop
  //if true, check if msg has restricted to streamer or mods
  //if resticted, check user role
  //if user is not authorized, drop
  //request sent to desired sevice
  //response twitch bot
});

app.post("/commands/update", async (req, res) => {
  //get service from IP
  //get command list and restriction from req.body
  //batch update force replace all from the command list
  //response success
});

module.exports = app;
